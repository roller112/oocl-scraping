const puppeteer = require('puppeteer');
const moment = require('moment');
const parseString = require('xml2js').parseString;
const readline = require('readline');

const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const scrappingURL = 'https://www.oocl.com/eng/ourservices/eservices/cargotracking/Pages/cargotracking.aspx?site=uk&lang=eng';

// let searchKeyword = 'OOLU2615768260';
let searchKeyword = '';

let scrape = async () => {
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    ignoreHTTPSErrors: true,
    headless: true,
    args: [
      '--disable-gpu',
      '--disable-software-rasterizer',
      '--disable-setuid-sandbox',
      '--no-sandbox',
      '--disable-extensions',
      '--remote-debugging-port=9222',
      '--disable-translate'
    ]
  });

  console.log('[---Open Browser---]');
  const page = await browser.newPage();
  await page.setViewport({ width: 1500, height: 3000 });

  console.log('[---Open new Page---]');
  console.log('[---Enter URL---]');
  await page.goto(scrappingURL);
  
  console.log('[---Wait 2 seconds---]');
  await page.waitFor(5000);

  console.log('[---Wait for loading---]');
  try {
    await page.waitForSelector('.dropdown-toggle', {
      timeout: 20000
    });

    await page.screenshot({path: 'Loading.png'});
    console.log('[---Screenshot Taken---]');
  } catch (error) {
    console.log('[---Loading failed---]');
  }


  // Click TrackingType dropdown
  console.log('[---Click TrackingType dropdown---]');
  const trackingTypeButton = await page.$$('.dropdown-toggle');
  await trackingTypeButton[0].click();

  // Select B/L from dropdown
  console.log('[---Select B/L from dropdown---]');
  const dropdownItemButton = await page.$$('div.cargoTrackingDropDrown li a');
  await dropdownItemButton[0].click();


  // Search input
  console.log('[---Search Input box---]');
  const searchInput = await page.$$('#SEARCH_NUMBER')
  await searchInput[0].type(searchKeyword);

  // Click search button
  console.log('[---Click search button---]');
  const searchButton = await page.$$('#container_btn');
  await searchButton[0].click();

  await page.waitFor(5000);
  
  // Cookie Buttons
  console.log('[---Search cookie buttons---]');
  const cookieButtons = await page.$$('#btn_cookie_accept');
  console.log('[---cookiebuttoncount---]', cookieButtons.length);
  if (cookieButtons.length > 1) {
    console.log('[---Click cookie accept button---]');
    // cookieButtons[1].click();
    cookieButtons[0].click();
  }

  await page.waitFor(2000);

  await page.screenshot({path: 'Loading11.png'});
  console.log('[---Screenshot Taken---]');



  // New Input

  // Click TrackingType dropdown
  console.log('[---Click TrackingType dropdown---]');
  const trackingTypeButton1 = await page.$$('.dropdown-toggle');
  await trackingTypeButton1[0].click();

  // Select B/L from dropdown
  console.log('[---Select B/L from dropdown---]');
  const dropdownItemButton1 = await page.$$('div.cargoTrackingDropDrown li a');
  await dropdownItemButton1[0].click();


  // Search input
  console.log('[---Search Input box---]');
  const searchInput1 = await page.$$('#SEARCH_NUMBER')
  await searchInput1[0].type(searchKeyword);

  // Click search button
  console.log('[---Click search button---]');
  const searchButton1 = await page.$$('#container_btn');
  await searchButton1[0].click();

  // await page.waitFor(5000);

  ///

  


  console.log('[---Get popup window---]');
  const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
  const popup = await newPagePromise;
  // console.log(popup.url());

  console.log('[---Wait for table loading---]');
  try {
    await popup.waitForSelector('#contentTable', {
      timeout: 20000
    });

    await popup.screenshot({path: 'Loading12.png'});
    console.log('[---Screenshot Taken---]');
  } catch (error) {
    console.log('[---Loading failed---]');
  }

  // const resultTables = await popup.$$('table#contentTable tbody:first-child tr:nth-child(2) td:first-child table:first-child tbody tr:first-child td:nth-child(2) table:nth-child(2) tbody tr:nth-child(2) td:first-child table:nth-child(2) tbody tr:nth-child(4) td:first-child  table table table');
  const resultTables = await popup.$$('#Tab1 #eventListTable');
  console.log('[---tablelength---]', resultTables.length);

  let containerDatas = [];
  for (let i=0; i < resultTables.length; i++) {
    const resultHtml = await popup.evaluate(table => table.innerHTML, resultTables[i]);
    containerDatas.push(getContainerData(resultHtml));
  }

  let output = {};

  if (containerDatas.length > 0) {
    output = {
      bl: searchKeyword,
      vesselName: containerDatas[0].currentVessel.trim(),
      voyageNumber: containerDatas[0].currentVoyage,
      eta: containerDatas[0].estimatedTimeOfArrival,
      status: containerDatas[0].status,
      locations: [
        containerDatas[0].original,
        containerDatas[0].currentLocation,
        containerDatas[0].destination
      ]
    }
    console.log('[---output---]', output);
  } else {
    console.log('[---Shipment Not found---]');
  }

  console.log('[---Close browser---]');
  browser.close();
};
  
r1.question('Input tracking number, container number or BOL number:', (input) => {
  console.log('[---Searching for ---]', input);
  searchKeyword = input;

  if (input) {
    console.log('[---Start scrapping----]');
    scrape().then((value) => {
      // console.log(value); // Success!
    });
  }

  r1.close();
})

async function getElementsFrom(page, selector) {
  const ids = await page.evaluate(() => {
    const list = document.querySelectorAll(selector);
    const ids = [];
    for (const element of list) {
      const id = selector + ids.length;
      ids.push(id);
      element.setAttribute('puppeteer', id);
    }
    return ids;
  }, selector);
  const getElements = [];
  for (const id of ids) {
    getElements.push(page.$(`${selector}[puppeteer=${id}]`));
  }
  return Promise.all(getElements);
}


function getContainerData(tempData) {
  let convertedString = tempData.replace(/\r?\n|\r/g, '');
  convertedString = convertedString.replace(/<br>/g, '<br></br>');
  convertedString = convertedString.replace(/<\/a>/g, '</img></a>');
  let resultRows = [];

  let estimatedTimeOfArrival = '';
  let currentLocation = '';
  let destination = '';
  let originalLocation = '';
  let currentVessel = '';
  let currentVoyage = '';
  let status = 'IN-DELIVERY';

  parseString(convertedString, (err, result) => {
    let trs = result.tbody.tr;
    
    trs = trs.map(item => {
      let tds = item.td;
      if (tds) {
        const rowData = {
          origin: tds[0].span ? tds[0].span[0]._ : "",
          emptyPickup: tds[1].span ? tds[1].span[0]._ : "",
          fullReturn: {
            one: tds[2].span ? tds[2].span[0]._ : "",
            two: tds[2].span ? tds[2].span[1]._ : ""
          },
          portOfLoad: {
            one: tds[3].span ? tds[3].span[0]._ : "",
            two: tds[3].span ? tds[3].span[1]._ : ""
          },
          vesselVoyage: tds[4]._.replace(/\t/g, '').replace('()', '').trim(),
          portOfDischarge: {
            one: tds[5].span ? tds[5].span[0]._ : "",
            two: tds[5].span ? tds[5].span[1]._ : ""
          },
          finalDistination: tds[6].span ? tds[6].span[0]._ : "",
          destination: tds[7].span ? tds[7].span[0]._ : "",
          emptyReturnLocation: tds[8].span ? tds[8].span[0]._ : ""
        }
        resultRows.push(rowData);
      }
    });
  })

  if (resultRows.length === 0) {
    return null;
  }

  estimatedTimeOfArrival = resultRows[1].finalDistination;

  destination = resultRows[1].destination;
  originalLocation = resultRows[0].origin;
  currentLocation = resultRows[0].portOfDischarge.one;
  currentVessel = resultRows[1].vesselVoyage.replace('  ', ' ').replace('  ', ' ').replace('  ', ' ');
  currentVessel = currentVessel.split(' ');
  currentVoyage = currentVessel[currentVessel.length - 1];
  currentVessel = currentVessel.slice(0, currentVessel.length - 1).join(' ');

  let cDate = moment(estimatedTimeOfArrival, 'DD MMM YYYY, HH:mm');
  const nDate = new Date();
  if (cDate <= nDate) {
    currentLocation = destination;
    status = 'COMPLETE';
  }

  estimatedTimeOfArrival = moment(estimatedTimeOfArrival,'DD MMM YYYY, HH:mm').format(moment.HTML5_FMT.DATETIME_LOCAL_MS);

  return {
    estimatedTimeOfArrival,
    currentLocation: splitLocation(currentLocation, true),
    currentVessel,
    currentVoyage,
    destination: splitLocation(destination),
    original: splitLocation(originalLocation),
    status
  };
}

function splitLocation(location, isCurrent) {
  let newLocation = location;
  if (location) {
    newLocation = location.split(', ');
    if (newLocation.length === 2) {
      if (isCurrent) {
        return {
          isCurrent: true,
          city: newLocation[0],
          state: '',
          country: newLocation[1]
        }
      }
      return {
        city: newLocation[0],
        state: '',
        country: newLocation[2]
      }
    }
    if (isCurrent) {
      return {
        isCurrent: true,
        city: newLocation[0],
        state: newLocation[1],
        country: newLocation[2]
      }
    }
    return {
      city: newLocation[0],
      state: newLocation[1],
      country: newLocation[2]
    }
  }
  if (isCurrent) {
    return {
      isCurrent: true,
      location
    }
  }
  return {
    location
  };
}
